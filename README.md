# Amplemarket Code Challenge

Contains the code for the Amplemarket code challenge.

## Template Extension

The goal of this project is to build a Gmail extension that adds support for basic templates.

## Challenge

People who spend a lot of time writing emails (sales reps, customer support reps, etc) know that you end up writing the same things over and over again. Templates help folks be much more efficient. By the end of this challenge, you will have a chrome extension that allows you to easily insert a reply template into a message draft within Gmail with just a couple of clicks.

### Required Features

* Adds a button next to the Gmail send button. When you click this button a modal opens which will show a selection of default templates (the UI doesn’t need to look exactly like the one in the picture above - tho the modal should show up next to the button that triggers it)

* Within the **templates modal there should be an iframe with the src set to a url pointing to the backend (e.g. [https://xyz-amplemarket-challenge.herokuapp.com/templates](https://xyz-amplemarket-challenge.herokuapp.com/templates) or** https://localhost:3000/templates**). **We know there are other ways of implementing this, but we would like you to do it via an iframe.**

* When you click one of the templates in the list, it will insert the text of the template into the body of the draft (replacing any existing text there)

* The extension **should work for a personal Gmail account - @gmail.com.** (vs a Google Mail on some other domain).

### Extra Features

* Preview of the templates, by the use of an `<Accordion />` component

* Option to create new templates

## Structure

The app has the following structure:

```
app/
 ├── public/
      ├── assets/
      └── index.html
      └── index.html
      src/
      ├── components/ ................. the app building blocks
      │   └── ...
      │-- contracts/ .................. commom types/interfaces used in the application
      │-- enhancers/
      │   └── hooks/ ................... custom Hooks
      ├── screens/ ..................... all the view the application contains
      │   └── ...
      ├── states/ ...................... application states, like the context API
      │   └── ...
      ├── index.tsx ................... app entry point
      └── setupTests.ts ............... setting up the testing library
extension/
 ├── assets/
      └── icons ....................... the icons used for the extension
 ├──  app.js ........................... this file configures the connection with the UI application
 ├──  inboxsdk.js ...................... Inbox SDK library file
 └── manifest.json
```

## How to Run Locally

1. Project dependencies

```bash
cd app && npm i
```

2. Running the app locally.

```bash
npm start
```

## How to View the Extension in Gmail

1. Go to the [chrome://extensions/](chrome://extensions/)

2. Check the "Developer Mode" checkbox.

3. Click on "Load Unpacked Extension" and point it to the extension folder.

4. Open [https://mail.google.com/](https://mail.google.com/) and click on "+ Compose", there should be a new button that will allow to add the templates.


## Technologies Used

### Create React App
A simple way to setup React without having to configurate Webpack.

### TypeScript
Decided to use TypeScript so it's easy to find and fix type errors.

### CSS Modules
Easy way to make visual changes and keep the scope of the styles.

### InboxSDK
A library for building browser extensions for Gmail.

## Skipped Due to Time Constrains
- Loading status
- Testing components
- Option to edit the templates already created

## Future Work
- Use animations
- Let the user filter the templates (Search, Category, ID)