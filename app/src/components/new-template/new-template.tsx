import React, { FunctionComponent, useEffect, useState } from 'react';

import { Template } from '../../contracts/template';
import Button from '../button';

import styles from './new-template.module.css';

const placeholders = {
  title: 'Template title',
  body: 'Dear [name],\n\nRegards, ...',
};

const minLengths = {
  title: 3,
  body: 10,
};

type NewTemplateType = Pick<Template, 'title' | 'body'>;

interface INewTemplate {
  onSave: ({ title, body }: { title: string; body: string }) => void;
  onCancel: () => void;
}

const NewTemplate: FunctionComponent<INewTemplate> = ({ onCancel, onSave }) => {
  const [saveEnable, setSaveEnable] = useState(false);
  const [template, newTemplate] = useState<NewTemplateType>({
    title: '',
    body: '',
  });

  useEffect(() => {
    const isValid =
      template.body.length >= minLengths.body &&
      template.title.length >= minLengths.title;
    setSaveEnable(isValid);
  }, [template]);

  const handleOnChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    newTemplate({
      ...template,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnSave = () => {
    onSave(template);
  };

  const handleOnCancel = () => {
    onCancel();
  };

  return (
    <div className={styles.container}>
      <div className={styles['content-wrapper']}>
        <input
          className={styles.input}
          type="text"
          value={template.title}
          name="title"
          placeholder={placeholders.title}
          onChange={handleOnChange}
        />
        <textarea
          className={styles.input}
          rows={5}
          value={template.body}
          name="body"
          placeholder={placeholders.body}
          onChange={handleOnChange}
        />
      </div>
      <div className={styles['buttons-wrapper']}>
        <Button onClick={handleOnCancel} varient='secondary'>Cancel</Button>
        <Button onClick={handleOnSave} disabled={!saveEnable}>
          Save
        </Button>
      </div>
    </div>
  );
};

export type { NewTemplateType, INewTemplate };
export default NewTemplate;
