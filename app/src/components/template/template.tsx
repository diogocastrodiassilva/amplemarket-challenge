import React, { FunctionComponent } from 'react';

import Accordion from '../accordion';

import styles from './template.module.css';

interface ITemplate {
  id: string;
  title: string;
  body: string;
  expanded: boolean;
  onExpand: (isExpanded: boolean) => void;
  onSelect: (id: string) => void;
}

const Template: FunctionComponent<ITemplate> = ({
  id,
  title,
  body,
  expanded,
  onExpand,
  onSelect,
}) => {
  const handleOnSelect = () => {
    onSelect(id);
  };

  const handleOnInformationClick = (newStatus: boolean) => {
    onExpand(newStatus);
  };

  return (
    <Accordion onExpand={handleOnInformationClick} expanded={expanded}>
      <Accordion.Header>
        <h5 onClick={handleOnSelect} className={styles.title}>
          {title}
        </h5>
      </Accordion.Header>
      <Accordion.Item>
        <p className={styles['body-text']}>{body}</p>
      </Accordion.Item>
    </Accordion>
  );
};

export default Template;
