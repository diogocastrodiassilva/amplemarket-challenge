import React, { FunctionComponent } from 'react';

import useAccordion from '../../../enhancers/hooks/use-accordion';

import styles from './header.module.css';

const Header: FunctionComponent = ({ children }) => {
   const { onExpand } = useAccordion();

   const handleOnClick = () => {
      onExpand();
   };

   return (
     <div className={styles.wrapper}>
       {children}
       <img
         onClick={handleOnClick}
         src="https://cdn-icons-png.flaticon.com/512/167/167801.png"
         alt="information icon"
         className={styles.icon}
       />
     </div>
   );
};

export default Header;
