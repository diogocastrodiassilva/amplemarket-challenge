import React, { FunctionComponent } from 'react';

import useAccordion from '../../../enhancers/hooks/use-accordion';

import styles from './item.module.css';

const Item: FunctionComponent = ({ children }) => {
  const { expanded } = useAccordion();

  return expanded ? <div className={styles.wrapper}>{children}</div> : null;
};

export default Item;
