import React, { FunctionComponent } from 'react';

import { AccordionProvider } from '../../states/accordion';

import Header from './header';
import Item from './item';
import styles from './accordion.module.css';

interface Iaccordion {
  onExpand: (expanded: boolean) => void;
  expanded: boolean;
}

interface IAccordionComposition {
  Header: FunctionComponent;
  Item: FunctionComponent;
}

const Accordion: FunctionComponent<Iaccordion> & IAccordionComposition = ({
  children,
  expanded,
  onExpand,
}) => {
  return (
    <AccordionProvider expanded={expanded} onExpand={onExpand}>
      <li className={styles.container}>{children}</li>
    </AccordionProvider>
  );
};

Accordion.Header = Header;
Accordion.Item = Item;

export type { Iaccordion };
export default Accordion;
