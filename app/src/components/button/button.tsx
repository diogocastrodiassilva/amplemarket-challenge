import React, { FunctionComponent } from 'react';

import styles from './button.module.css';

type Varients = 'primary' | 'secondary';

interface IButton extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  varient?: Varients;
}

const Button: FunctionComponent<IButton> = ({ children, varient = 'primary', ...rest }) => {
  const className = `${styles.button} ${styles[varient]}`

  return (
    <button className={className} {...rest}>
      {children}
    </button>
  );
};

Button.defaultProps = {
  varient: 'primary',
};

export default Button;
