import React, { useContext } from 'react';

import { IAccordionContext, AccordionContext } from '../../states/accordion';

const useAccordion = (): IAccordionContext => {
   const context = useContext(AccordionContext);

   if (!context) {
      throw new Error('useAccordion must be used within an AccordionProvider');
   }

   return context;
};

export default useAccordion;
