interface Template {
  id: string;
  title: string;
  body: string;
}

export type { Template };
