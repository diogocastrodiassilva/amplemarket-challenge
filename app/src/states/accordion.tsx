import React, { createContext, FunctionComponent } from 'react';

interface IAccordionContext {
  expanded: boolean;
  onExpand: () => void;
}

interface IAccordionProvider {
  expanded: boolean;
  onExpand: (isExpanded: boolean) => void;
}

const AccordionContext = createContext<IAccordionContext>({
  expanded: false,
  onExpand: () => {},
});

const AccordionProvider: FunctionComponent<IAccordionProvider> = ({
  children,
  expanded,
  onExpand,
}) => {
  const [isExpanded, setIsExpanded] = React.useState<boolean>(false);

  React.useEffect(() => {
    setIsExpanded(expanded);
  }, [expanded]);

  const handleOnToogle = () => {
    setIsExpanded(!isExpanded);

    if (onExpand) {
      onExpand(!isExpanded);
    }
  };

  return (
    <AccordionContext.Provider
      value={{ expanded: isExpanded, onExpand: handleOnToogle }}
    >
      {children}
    </AccordionContext.Provider>
  );
};

export type { IAccordionContext };
export { AccordionContext, AccordionProvider };
