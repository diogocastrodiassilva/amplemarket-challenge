import React, { FunctionComponent } from 'react';
import Button from '../../components/button';

import Body from './body';
import styles from './templates.module.css';

const Templates: FunctionComponent = () => {
  const [newTemplate, setNewTemplate] = React.useState(false);

  const handleNewTemplate = (status: boolean) => {
    setNewTemplate(status);
  }

  return (
    <div className={styles.container}>
      <h4 className={styles.title}>Personal Snippets</h4>
      <Body
        isToCreateNewTemplate={newTemplate}
        closeNewTemplate={() => handleNewTemplate(false)}
      />
      <div className={styles.footer}>
        <Button onClick={() => handleNewTemplate(true)}>
          Create new template
        </Button>
      </div>
    </div>
  );
};

export default Templates;
