import React, { FunctionComponent } from 'react';

import NewTemplate from '../../../components/new-template';
import { NewTemplateType } from '../../../components/new-template/new-template';
import Template from '../../../components/template';
import { Template as ITemplate } from '../../../contracts/template';

const templates: ITemplate[] = [
  {
    id: '1',
    title: 'New Job - Team Member',
    body: `Hey [Department] team,

Hope you’re all having a great day!

I’m [Your Name] and I’m the new [job title] here. Since I know we’ll be working together on quite a few different projects, I wanted to reach out and briefly introduce myself.

I’m super excited to work with you all and am looking forward to meeting you personally during our upcoming meeting on [date].

See you soon,
[Your Name]`,
  },
  {
    id: '2',
    title: 'New Job - Department',
    body: `Hey [Name],

I know that we’ve already been briefly introduced, but I just wanted to send you a quick note to say that I’m really looking forward to working with you here!

I’d love to find a time when we can grab lunch or a quick coffee to chat and get to know each other a little better.

Does [day] at [time] work for you? It’s my treat!

Let me know,
[Your Name]`,
  },
  {
    id: '3',
    title: 'Thank You For the Interview',
    body: `Hi [Interviewer Name],

Thank you so much for meeting with me today. It was such a pleasure to learn more about the team and position, and I’m very excited about the opportunity to join [Company Name] and help [bring in new clients/develop world-class content/anything else awesome you would be doing] with your team.

I look forward to hearing from you about the next steps in the hiring process, and please do not hesitate to contact me if I can provide additional information.

Best regards,
[Your Name]`,
  },
  {
    id: '4',
    title: 'Out of the Office',
    body: `Hi there,

Thank you for your email. I will be out of the office from [mm/dd] to [mm/dd] and will not have access to email. If this is urgent, please contact [NAME] at [EMAIL] or [PHONE]. I will do my best to respond promptly to your email when I return on [mm/dd].

Best regards,

[Your Name]`,
  },
];

interface IBody {
  isToCreateNewTemplate: boolean;
  closeNewTemplate: () => void;
}

const Body: FunctionComponent<IBody> = ({
  isToCreateNewTemplate,
  closeNewTemplate,
}) => {
  const [expandedTemplate, setExpandedTemplate] = React.useState<string | null>(
    null
  );

  const handleOnExpand = (id: string) => (isExpanded: boolean) => {
    setExpandedTemplate(isExpanded ? id : null);
  };

  const handleOnSelect = (id: string) => {
    const selectedTemplate = templates.find((template) => template.id === id);

    if (selectedTemplate && window?.top) {
      window.top.postMessage(selectedTemplate?.body, '*');
    }
  };

  const handleOnSave = ({ title, body }: NewTemplateType) => {
    const newTemplate: ITemplate = {
      id: (templates.length + 1).toString(),
      title,
      body,
    };

    templates.push(newTemplate);

    closeNewTemplate();
  };

  const handleOnCancel = () => {
    closeNewTemplate();
  };

  return (
    <div>
      {isToCreateNewTemplate && <NewTemplate onSave={handleOnSave} onCancel={handleOnCancel} />}
      <ul>
        {templates.map(({ id, title, body }) => (
          <Template
            key={id}
            id={id}
            title={title}
            body={body}
            expanded={expandedTemplate === id}
            onExpand={handleOnExpand(id)}
            onSelect={handleOnSelect}
          />
        ))}
      </ul>
    </div>
  );
};

export default Body;
