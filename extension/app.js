const appUrl = 'http://localhost:3000';

InboxSDK.load(2, 'sdk_diogo-amplemark_df8f7033a5').then(function (sdk) {
  sdk.Compose.registerComposeViewHandler(function (composeView) {
    composeView.addButton({
      title: 'Templates',
      color: 'blue',
      iconUrl: 'https://cdn-icons-png.flaticon.com/512/759/759786.png',
      hasDropdown: true,
      onClick: function (event) {
        event.dropdown.el.innerHTML = `<iframe src='${appUrl}/templates' width="500" height="500"></iframe>`;
      },
    });

    window.addEventListener('message', (event) => {
      if (event?.origin === appUrl && event?.data) {
        composeView.setBodyText(event.data);
      }
    });
  });
});
